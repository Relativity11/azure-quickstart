variable "RESOURCEGROUP" {
  type = map(any)
  default = {
    name     = "infraRG"
    location = "westus"
  }
}

variable "VIRTUALNETWORK" {
    type = map(any)
    default = {
      VNET_name = "infraNET"
      VNET_ad_space = "10.0.0.0/16"
    }
}

variable "SUBNET1" {
    type = map(any)
    default = {
        name = "infrasub1"
        address_prefix = "10.0.1.0/24"
    }  
}

variable "SUBNET2" {
    type = map(any)
    default = {
        name = "infrasub2"
        address_prefix = "10.0.2.0/24"
    }
}

variable "STORAGEACCOUNT" {
  type = map(any)
  default = {
    name = "infrastore"
  }
}

variable "STORAGECONTAINER" {
  type = map(any)
  default = {
    name = "infracontainer"
  }
}