resource "azurerm_storage_account" "STORAGEACCOUNT" {
    name = var.STORAGEACCOUNT.name
    resource_group_name = azurerm_resource_group.RESOURCEGROUP.name
    location = azurerm_resource_group.RESOURCEGROUP.location
    account_tier = "Standard"
    account_replication_type = "LRS"
}

#Include Storage Container

resource "azurerm_storage_container" "STORAGECONTAINER" {
    name = var.STORAGECONTAINER.name
    storage_account_name = azurerm_storage_account.STORAGEACCOUNT.name
    container_access_type = "private"
}