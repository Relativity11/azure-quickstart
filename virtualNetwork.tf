resource "azurerm_virtual_network" "VNET" {
    name = var.VIRTUALNETWORK.VNET_name
    address_space = [var.VIRTUALNETWORK.VNET_ad_space]
    location = azurerm_resource_group.RESOURCEGROUP.location
    resource_group_name = azurerm_resource_group.RESOURCEGROUP.name
}